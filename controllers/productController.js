// Export the product schema model
const Product = require("../models/Product");

// Add User and Order later

module.exports.addProduct = (enterDetails)=>{				// Edit this later to admin access once may suth.js na
	let newProduct = new Product({
		name: enterDetails.name,
		description: enterDetails.description,
		price: enterDetails.price
	})
	return newProduct.save()
	.then((result, error)=>{
		if(error){
			return false
		}else{
			return result
		}
	})
}