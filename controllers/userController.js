// Export the schema models
const User = require("../models/User");

// Add Product and Order later

// Add bcrypt and auth later 

module.exports.createUser = (enterDetails) =>{
	let newUser = new User ({
		firstName: enterDetails.firstName,
		lastName: enterDetails.lastName,
		email: enterDetails.email,
		password: enterDetails.password 		// I will use hash to cover the password
	})
	return newUser.save()
	.then((result, error)=>{
		if(error){
			return false;
		}else{
			return result
		}
	})
}

// Users Ordered products
