// Export the schema models
const Order = require("../models/Order");
const User = require("../models/User");
const Product = require("../models/Product");


module.exports.successfulOrder = async(data)=>{
	let userWhoOrder = await User.findById(data.userId)
	.then(result =>{
		result.customerOrder.push({userId: data.userId})
	return result.save()
	.then((order, error)=>{
		if(error){
			return false;
		}else{
			return order
		}
	})
})
	let usersOrder = await Product.findById(data.productId)
	.then(result =>{
		result.customerOrder.push({productId: data.productId}, {totalAmount: data.price})

	return result.save()
	.then((order, error)=>{
		if(error){
			return false;
		}else{
			return order
		}
	})
})

	if(userWhoOrder && usersOrder){
		return true;
	}else{
		return false;
	}

}