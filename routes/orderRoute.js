// Require express
const express = require("express");
// Create a router
const router = express.Router();

// Export the user controller
const orderController = require("../controllers/orderController");
// Export auth for user access

// Create a User Order
router.post("/successful", (req, res)=>{
	let data = {
		userId: req.body.userId,
		productId: req.body.productId,
		price: req.body.price
	}
	console.log(data)
	orderController.successfulOrder(data)
	.then(result =>
		res.send(result))
})


module.exports = router;