// Require express
const express = require("express");
// Create a router
const router = express.Router();

// Export the product controller
const productController = require("../controllers/productController");

// create a product 							// Edit this later to admin access once may suth.js na
router.post("/create", (req, res)=>{
	productController.addProduct(req.body)
	.then(createdProd =>
		res.send(createdProd))
})

module.exports = router;