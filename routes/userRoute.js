// Require express
const express = require("express");
// Create a router
const router = express.Router();

// Export the user controller
const userController = require("../controllers/userController");
// Export auth for user access


// Create a User
router.post("/create", (req, res)=>{
	userController.createUser(req.body)
	.then(userCreated =>
		res.send(userCreated))
})

// Users Ordered products
router.post("/successful", (req, res)=>{
	userController.userOrder(req.body)
	.then(orders =>
		res.send(orders))
})

module.exports = router;